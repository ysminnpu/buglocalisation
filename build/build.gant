#!groovy
/**
 * See LICENCE_BSD for licensing information
 *
 * Copyright Steven Davies 2012
 */

includeTool << gant.tools.Ivy

property(name: 'root', value:'..')
property(file: "${root}/build/build.properties")

java_dir = "${root}/java/"
java_src_dir = "${java_dir}/src/"
java_test_dir = "${java_dir}/test/"
java_build_dir = "${java_dir}/build/"
python_dir = "${root}/python"
python_src_dir = "${python_dir}/src"
python_test_dir = "${python_dir}/test"
perl_dir = "${root}/perl/src"
test_results_dir = "${root}/test_results/"
preliminary_report_dir = "${root}/latex/preliminary"
final_report_dir = "${root}/latex/final"
presentation_dir = "${root}/latex/presentation"

path(id: 'compile_path'){
    fileset(dir: "${java_dir}/lib"){
        include(name:"**/*.jar")
    }
}
ant.property(name:"compile_path", refid:"compile_path")

path(id: 'run_classpath'){
    path(refid: 'compile_path')
    pathelement(path: java_build_dir)
}
ant.property(name:"run_classpath", refid:"run_classpath")

path(id: 'pythontestpath'){
    pathelement(path: python_src_dir)
}
ant.property(name:"pythontestpath", refid:"pythontestpath")

def debug_project = {
    project.each { k, v ->
        println(k + ': ' + v)
    }
}

def run_java(classname) {
    depends(compile)
    ant.java(classname: classname, classpathref: 'run_classpath', failonerror: true, fork: true, maxmemory:'1024M'){
        arg(file: 'build.properties')
        arg(file: project['config'])
    }
}

def run_python(module, args={}) {
    ant.exec(executable:'python', dir: python_src_dir, failonerror: true){
        arg(value:module, prefix:'-m')
        arg(file: 'build.properties', prefix:'--config=')
        arg(file: 'default.config', prefix:'--config=')
        arg(file: project['config'], prefix:'--config=')
        args()
    }
}

def find_fixes = {
    depends(uncache)
    run_python('events/BugFixFinder')
}

def download_bugs = {
    depends(uncache)
    run_python('events/BugDownloader')
}

def find_state = {
    depends(uncache)
    run_python('events/BugStateFinder')
}

def events = {
    depends(uncache)
    run_python('events/EventLogCreator')
}

def process = {
    depends(compile)
    depends(uncache)
    run_python('events/EventLogProcessor'){
        arg(value: run_classpath, prefix:'--path-java=')
        arg(file: perl_dir, prefix:'--path-pl=')
    }
}

def resume = {
    depends(compile)
    depends(uncache)
    run_python('events/EventLogProcessor'){
        arg(value: run_classpath, prefix:'--path-java=')
        arg(file: perl_dir, prefix:'--path-pl=')
        arg(value: '--resume')
    }
}

def process_from = {
    depends(compile)
    depends(uncache)
    run_python('events/EventLogProcessor'){
        arg(value: run_classpath, prefix:'--path-java=')
        arg(file: perl_dir, prefix:'--path-pl=')
        arg(value: event, prefix:'--start-event=')
    }
}

def reprocess = {
    depends(compile)
    depends(uncache)
    run_python('events/EventLogProcessor'){
        arg(value: run_classpath, prefix:'--path-java=')
        arg(file: perl_dir, prefix:'--path-pl=')
        arg(value: 0, prefix:'--start-event=')
        arg(value: fix_plugins, prefix:'--override=plugins.FIX:')
        arg(value: report_plugins, prefix:'--override=plugins.REPORT:')
    }
}

def resume_reprocess = {
    depends(compile)
    depends(uncache)
    run_python('events/EventLogProcessor'){
        arg(value: run_classpath, prefix:'--path-java=')
        arg(file: perl_dir, prefix:'--path-pl=')
        arg(value: '--resume')
        arg(value: fix_plugins, prefix:'--override=plugins.FIX:')
        arg(value: report_plugins, prefix:'--override=plugins.REPORT:')
    }
}

target(debug:'Prints debugging information'){
    echoproperties()
}

target(get_jars:'Downloads java dependencies'){
    property(name:'ivy.dep.file', location:"${root}/build/ivy.xml")
    ivy.retrieve(pattern:"${java_dir}/lib/ivy/[artifact]-[revision].[ext]", type:'jar,eclipse-plugin')
}

target(get_modules:'Downloads python dependencies'){
    ant.exec(executable:'pip', failonerror:true){
        arg(value:'install')
        arg(file:"${root}/build/requirements.pip.1", prefix:'--requirement=')
    }
    ant.exec(executable:'pip', failonerror:true){
        arg(value:'install')
        arg(file:"${root}/build/requirements.pip.2", prefix:'--requirement=')
    }
}

target(compile:'Compiles Java classes'){
    depends(get_jars)
    mkdir(dir:java_build_dir)
    javac(srcdir:java_src_dir, destdir: java_build_dir, classpathref:'compile_path', debug:true) {
        include(name: '**/*.java')
        exclude(name: 'uk/ac/strath/cis/spd/buglanguage/evaluate/**')
    }
}

target(test: 'Runs all tests'){
    depends(test_java)
    depends(test_python)
}

target(test_java:'Tests Java classes'){
    depends(compile)
    mkdir(dir:test_results_dir)
    taskdef ( name:'coverage', classname:'org.jacoco.ant.CoverageTask', classpathref:'compile_path' )
    taskdef ( name:'coverage_report', classname:'org.jacoco.ant.ReportTask', classpathref:'compile_path' )
    javac(srcdir:java_test_dir, destdir: java_build_dir, classpathref:'compile_path', debug:true) {
        include(name: '**/*.java')
    }
    coverage(destfile:"${test_results_dir}/jacoco.exec"){
        junit(fork:true, dir:java_dir){
            classpath(refid:'run_classpath')
            formatter(type:'plain', usefile:false)
            formatter(type:'xml')
            batchtest(todir:test_results_dir){
                fileset(dir:java_test_dir){
                    include(name: '**/*Test.java')
                }
            }
        }
    }
    coverage_report(){
        executiondata(){
            file(file:"${test_results_dir}/jacoco.exec")
        }
        structure(name:'uk.ac.cis.strath.spd.buglanguage'){
            classfiles(){
                fileset(dir:java_build_dir)
            }
            sourcefiles(){
                fileset(dir:java_src_dir)
            }
        }
        xml(destfile:"${test_results_dir}/jacoco.xml")
    }
}

target(test_python:'Tests Python modules'){
    depends(get_modules)
    mkdir(dir:test_results_dir)
    ant.exec(executable:'nosetests', dir: python_dir, failonerror: true){
        env(key:'PYTHONPATH', path:pythontestpath)
        arg(value:"TestResultParser\$", prefix:'--exclude=')
        arg(value:"TestRunner\$", prefix:'--exclude=')
        arg(value:'--with-xunit')
        arg(file:"${test_results_dir}/nosetests.xml", prefix:'--xunit-file=')
        arg(value:'--with-xcoverage')
        arg(value:'--cover-inclusive')
        arg(value:'--cover-tests')
        arg(value:'--cover-erase')
        arg(value:'--cover-branches')
        arg(value:"bug_parsers", prefix:'--cover-package=')
        arg(value:"events", prefix:'--cover-package=')
        arg(value:"plugins", prefix:'--cover-package=')
        arg(file:"${test_results_dir}/coverage.xml", prefix:'--xcoverage-file=')
    }
}

target(uncache: 'Cleans Sweave cache whenever new reports are generated'){
    sweave_cache_dir = "${preliminary_report_dir}/cache"
    delete(dir:sweave_cache_dir)
    mkdir(dir:sweave_cache_dir)
    sweave_cache_dir = "${final_report_dir}/cache"
    delete(dir:sweave_cache_dir)
    mkdir(dir:sweave_cache_dir)
    sweave_cache_dir = "${presentation_dir}/cache"
    delete(dir:sweave_cache_dir)
    mkdir(dir:sweave_cache_dir)
}

def sweave(report_dir) {
    property(name: 'sweave_cache_dir', location:"${report_dir}/cache")

    tmp_dir = "${report_dir}/tmp"
    delete(dir:tmp_dir)
    delete(file:"${report_dir}/paper.tex")
    delete(file:"${report_dir}/paper.pdf")

    mkdir(dir:tmp_dir)
    mkdir(dir:sweave_cache_dir)
    exec(executable:'Sweave.sh', dir:report_dir, failonerror:true){
        env(key:'R_LOCAL_LIB', value:r_lib_dir)
        env(key:'R_RESULTS', value:results_dir)
        arg(value:"--cache=${sweave_cache_dir}")
        arg(value:'paper.Rnw')
    }
    exec(executable:'rubber', dir:report_dir, failonerror:true){
        env(key:'TEXMFOUTPUT', file:report_dir)
        arg(value:'--pdf')
        arg(value:'paper')
    }
    echo('Errors')
    exec(executable:'rubber-info', dir:report_dir){
        arg(value:'paper')
    }
    exec(executable:'pdftops', dir:report_dir, failonerror:true){
        arg(value:'paper.pdf')
        arg(value:'paper.ps')
    }
    exec(executable:'ps2pdf14', dir:report_dir, failonerror:true){
        arg(value:'-dPDFSETTINGS=/prepress')
        arg(value:'paper.ps')
        arg(value:'paper.pdf')
    }
    delete(file:"${report_dir}/paper.ps")
}

target(preliminary_report:'Generates preliminary report from evaluation files'){
    sweave(preliminary_report_dir)
}

target(final_report:'Generates final report from evaluation files'){
    sweave(final_report_dir)
}

target(presentation:'Generates presentation from evaluation files'){
    sweave(presentation_dir)
}

def targets = [
    [ name: 'debug', description: 'Prints info for', depends: debug_project ],
    [ name: 'find_fixes', description: 'Get list of bug-fixing commits for ', depends: find_fixes ],
    [ name: 'download_bugs', description: 'Downloads bug details for ', depends: download_bugs ],
    [ name: 'find_state', description: 'Determines current revision when bugs were reported for ', depends: find_state ],
    [ name: 'events', description: 'Creates event log for ', depends: events ],
    [ name: 'process', description: 'Processes event log for ', depends: process ],
    [ name: 'resume', description: 'Resumes processing of event log for ', depends: resume ],
    [ name: 'process_from', description: 'Processes event log, from event specified with -Devent=, for ', depends: process_from ],
    [ name: 'reprocess', description: 'Reprocesses particular plugins for event log, specified with -Dfix_plugins= and -Dreport_plugins=, for ', depends: reprocess ],
    [ name: 'resume_reprocess', description: 'Resumes reprocessing particular plugins for event log, specified with -Dfix_plugins= and -Dreport_plugins=, for ', depends: resume_reprocess ],
]

def projects = []
new File("${root}/build/").eachFile { file ->
    if(file.name.endsWith('.config') && file.name != 'default.config'){
        def project_name = file.name.replace('.config', '')
        projects << project_name
        target( (project_name): 'Builds ' + project_name ) {
            project = new Properties()
            new File("${root}/build/", file.name).withInputStream { project.load(it) }
            project['config'] = file.name
        }
        targets.each { t ->
            target( (t['name'] + '_' + project_name): (t['description'] + ' ' + project_name) ) {
                depends(project_name)
                (t['depends'])()
            }
        }
    }
}

targets.each { t ->
    target( (t['name'] + '_all'): (t['description'] + ' all projects') ) {
        projects.each {
            if(!(t['exclude'] && it.contains('_'))){
                depends(t['name'] + '_' + it)
            }
        }
    }
}
