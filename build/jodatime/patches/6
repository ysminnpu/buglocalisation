diff --git a/JodaTime/build.xml b/JodaTime/build.xml
index cac6cd9..464fcd1 100644
--- a/JodaTime/build.xml
+++ b/JodaTime/build.xml
@@ -38,7 +38,8 @@
 
   <!-- Dependencies -->
   <property name="lib"                     value="lib"/>
-  <property name="junit.jar"               value="${lib}/junit-3.8.1.jar"/>
+  <property name="junit.jar"               value="${lib}/junit-3.8.2.jar"/>
+  <property name="jodaconvert.jar"         value="${lib}/joda-convert-1.1.jar"/>
 
   <!-- The directory of source files -->
   <property name="source"                  value="src"/>
@@ -74,7 +75,7 @@
   <property name="compile.debuglevel"      value="lines,source"/>
 
   <!-- Should Java compilations set the 'deprecation' compiler option? -->
-  <property name="compile.deprecation"     value="true"/>
+  <property name="compile.deprecation"     value="false"/>
 
   <!-- Should Java compilations set the 'optimize' compiler option? -->
   <property name="compile.optimize"        value="true"/>
@@ -82,6 +83,7 @@
   <!-- Construct compile classpath -->
   <path id="compile.classpath">
     <pathelement location="${build.classes}"/>
+    <pathelement location="${jodaconvert.jar}"/>
   </path>
 
 
@@ -92,6 +94,7 @@
     <pathelement location="${build.classes}"/>
     <pathelement location="${build.tests}"/>
     <pathelement location="${junit.jar}"/>
+    <pathelement location="${jodaconvert.jar}"/>
   </path>
 
   <!-- Should all tests fail if one does? -->
@@ -120,6 +123,7 @@
     <filter token="version"               value="${component.version}"/>
     <available property="junit.ant" classname="junit.framework.Test"/>
     <available property="junit.present" file="${junit.jar}"/>
+    <available property="jodaconvert.present" file="${jodaconvert.jar}"/>
     <uptodate property="tz.build.notneeded" targetfile="${build.tz}/ZoneInfoMap" >
       <srcfiles dir= "${source.tz}" includes="**/*.*"/>
     </uptodate>
@@ -128,10 +132,16 @@
 <!-- ====================================================================== -->
 
   <target name="getjunit" unless="junit.present">
-    <echo message="Getting junit from http://www.ibiblio.org/maven/junit"/>
+    <echo message="Getting junit from http://repo2.maven.org/maven2/junit/junit/3.8.2"/>
     <setproxy />
     <mkdir dir="${lib}"/>
-    <get dest="${junit.jar}" usetimestamp="true" ignoreerrors="true" src="http://www.ibiblio.org/maven/junit/jars/junit-3.8.1.jar" />
+    <get dest="${junit.jar}" usetimestamp="true" ignoreerrors="true" src="http://repo2.maven.org/maven2/junit/junit/3.8.2/junit-3.8.2.jar" />
+  </target>
+  <target name="getjodaconvert" unless="jodaconvert.present">
+    <echo message="Getting joda-convert from http://repo1.maven.org/maven2/org/joda/joda-convert/1.1"/>
+    <setproxy />
+    <mkdir dir="${lib}"/>
+    <get dest="${jodaconvert.jar}" usetimestamp="true" ignoreerrors="true" src="http://repo1.maven.org/maven2/org/joda/joda-convert/1.1/joda-convert-1.1.jar" />
   </target>
 
 <!-- ====================================================================== -->
@@ -149,7 +159,7 @@
 
 <!-- ====================================================================== -->
 
-  <target name="prepare" depends="init,getjunit,installjunit"
+  <target name="prepare" depends="init,getjunit,installjunit,getjodaconvert"
           description="Prepare build directory">
     <mkdir dir="${build}"/>
     <mkdir dir="${build.classes}"/>
@@ -184,6 +194,9 @@
     	    source="1.5" target="1.5" includeantruntime="false">
       <classpath refid="compile.classpath"/>
     </javac>
+    <copy todir="${build.classes}">
+      <fileset dir="${source.home}" includes="**/*.properties"/>
+    </copy>
   </target>
 
 <!-- ====================================================================== -->
@@ -371,7 +384,7 @@
     <mkdir      dir="${build.dist.bundle}"/>
     <copy      file="LICENSE.txt" todir="${build.dist.bundle}"/>
     <copy      file="NOTICE.txt" todir="${build.dist.bundle}"/>
-    <copy      file="project.xml" todir="${build.dist.bundle}"/>
+    <copy      file="pom.xml" todir="${build.dist.bundle}"/>
     <copy      file="${build.fullname}.jar"
               todir="${build.dist.bundle}"/>
     <copy      file="${build.fullname}-sources.jar"
@@ -392,22 +405,43 @@
   </target>
 
   <target name="test.time" depends="compile.tests,compile.zoneinfo">
-    <echo message="Running time tests ..."/>
-    <junit printsummary="yes" haltonfailure="yes">
-      <formatter type="plain" usefile="false" />
-      <classpath>
-        <pathelement location="${build.classes}"/>
-        <pathelement location="${build.tests}"/>
-        <pathelement location="${build.tz}"/>
-        <pathelement path="${java.class.path}"/>
-      </classpath>
+      <echo message="Running time tests ..."/>
+      <junit printsummary="yes" haltonfailure="no">
+          <formatter type="plain" usefile="false" />
+          <classpath>
+              <pathelement location="${build.classes}"/>
+              <pathelement location="${build.tests}"/>
+              <pathelement location="${build.tz}"/>
+              <pathelement path="${java.class.path}"/>
+              <path refid="emma.lib"/>
+          </classpath>
+
+          <batchtest fork="yes">
+              <fileset dir="${test.home}">
+                  <include name="**/Test*.java"/>
+                  <include name="**/TestAll.java"/>
+              </fileset>
+          </batchtest>
+      </junit>
+  </target>
 
-      <batchtest fork="yes">
-        <fileset dir="${test.home}">
-          <include name="**/TestAll.java"/>
-        </fileset>
-      </batchtest>
-    </junit>
+  <property environment="env" />
+  <property name="emma.dir" value="${env.EMMA_HOME}" />
+  <path id="emma.lib" >
+      <pathelement location="${emma.dir}/emma.jar" />
+      <pathelement location="${emma.dir}/emma_ant.jar" />
+  </path>
+
+  <target name="emma" description="Instruments compiled classes" depends="compile,compile.tests">
+      <taskdef resource="emma_ant.properties" classpathref="emma.lib" />
+      <emma>
+          <instr mode="overwrite">
+              <instrpath>
+                  <pathelement location="${build.classes}"/>
+                  <pathelement location="${build.tests}"/>
+              </instrpath>
+          </instr>
+      </emma>
   </target>
 
   <!-- don't depend on jar, so we can test jar built on another JDK version -->
