use v5.10.0;
use strict;
use warnings;

use lscp qw(extractWords);
use Config::Properties;
use File::Copy;
use File::Path qw(make_path remove_tree);
use File::Slurp qw(read_file write_file);
use File::Spec::Functions;
use String::Util qw(trim);

my $config = new Config::Properties();
open(CONFIG, '<', $ARGV[0]);
# Config::Properties chokes on the starting [config] line, so read and discard the comments and that line
while(<CONFIG>){
    last unless /#/;
}
$config->load(*CONFIG);

my $initialise = $config->getProperty('initialise', '') eq 'true';
if($initialise){
    exit;
}

my $id = $config->getProperty('id');
my $current = $config->getProperty('current');
my $bug = $config->getProperty('bug');
my $debug = $config->getProperty('debug', '') eq 'true';
my $strip_lines = $config->getProperty('strip.lines', '') eq 'true';

my $in_file = catfile($current, 'temporary', 'BugUpdater.out', $bug);
if(! -e $in_file){
    say('[' . $id . '] No bug description for bug ' . $bug);
    exit;
}

my $out_dir = catfile($current, 'temporary', $id . '.out');
my $preprocessor = lscp->new;

$preprocessor->setOption("logLevel", $debug ? "info" : "error");
my %options = $config->properties;
while( my ($key, $value) = each %options){
    if($key =~ /^option\.(.+)$/){
        $preprocessor->setOption($1, $value)
    }
}

my $in_dir = catfile($current, 'temporary', $id . '.in');
if(-e $in_dir){
    remove_tree($in_dir) or die 'Cannot delete temporary folder';
}
make_path($in_dir) or die 'Cannot create temporary folder';
my $description = read_file($in_file) or die 'Cannot read bug description';
if($strip_lines){
    $description =~ s/\n//g;
}
write_file(catfile($in_dir, $bug), $description) or die 'Cannot create temporary file';

my @stopwords = ();
my $stopwords_fixed = $config->getProperty('stopwords.fixed', '');
if($stopwords_fixed){
    my @split = split(/,/, $stopwords_fixed);
    my @trimmed = map {trim($_)} @split;
    push(@stopwords, @trimmed);
}
my $stopwords_regex = $config->getProperty('stopwords.regex', '');
if($stopwords_regex){
    my @split = split(/,/, $stopwords_regex);
    my @trimmed = map {trim($_)} @split;
    my @quoted = map {qr/$_/} @trimmed;
    push(@stopwords, @quoted);
}
if(@stopwords){
    if($debug){
        say('[' . $id . '] Using stopwords ' . join(',', @stopwords));
    }
    $preprocessor->setOption("doStopwordsCustom", 1);
    $preprocessor->setOption("ref_stopwordsCustom", \@stopwords);
}

$preprocessor->setOption("inPath", $in_dir);
$preprocessor->setOption("outPath", $out_dir);
$preprocessor->preprocess();
