use v5.10.0;
use strict;
use warnings;

use lscp;
use Config::Properties;
use File::Copy;
use File::Spec::Functions;

my $config = new Config::Properties();
open(CONFIG, '<', $ARGV[0]);
# Config::Properties chokes on the starting [config] line, so read and discard the comments and that line
while(<CONFIG>){
    last unless /#/;
}
$config->load(*CONFIG);

my $initialise = $config->getProperty('initialise', '') eq 'true';
if($initialise){
    exit;
}

my $id = $config->getProperty('id');
my $current = $config->getProperty('current');
my $bug = $config->getProperty('bug');
my $debug = $config->getProperty('debug', '') eq 'true';

my $in_query = catfile($current, 'temporary', 'BugCleaner.corpus.out', $bug);
if(! -e $in_query){
    say('[' . $id . '] No query found');
    exit;
}

mkdir(catfile($current, $bug));
my $out_query = catfile($current, $bug, 'Preprocessor.query');
copy($in_query, $out_query) or die 'Could not copy query';
my $preprocessor = lscp->new;

$preprocessor->setOption("logLevel", $debug ? "info" : "error");
$preprocessor->setOption("inPath", catfile($current, 'temporary', 'MethodExtractor.out'));
$preprocessor->setOption("outPath", catfile($current, 'temporary', 'Preprocessor.out'));
$preprocessor->setOption("doTokenize", 1);
$preprocessor->setOption("doRemovePunctuation", 1);
$preprocessor->setOption("doLowerCase", 1);

# Under certain circumstances this emits 'Complex regular subexpression recursion limit exceeded'
# I think this is when the input file contains the pattern /* inside a String literal
# Because we're processing both identifiers and comments, I don't actually think it has much of an effect on the result
# It's possible I actually shouldn't be processing these files as code
$preprocessor->preprocess();
