'''
Created on 17 Jan 2013

@author: seb10184
'''
import unittest
from events.BugFixFinder import BugFixFinder
from os import path
import re
from test.BaseTest import BaseTest

class BugFixFinderTest(BaseTest):

    def test_execute(self):
        self.check_event_processor(BugFixFinder(),
                                   'test_execute',
                                   renames={path.join('test', 'events', 'project_src', 'dot_git'): path.join('test', 'events', 'project_src', '.git')},
                                   right_only=['test/events/project_src']
                                   )

    def test_bug_patterns(self):
        finder = BugFixFinder()
        finder.bug_pattern = re.compile('(?:^|[^0-9])([1-9][0-9]{5,6})(?=$|[^0-9])')
        actual = finder.find_bug_ids('Fix DateTimeZone.getDefault [3048468,3056104] Handle missing user.timezone system property on Android that resulted in stack overflow investigated by/patch suggeted by Matias Brunstein Macri')
        expected = ['3048468', '3056104']
        self.assertEquals(expected, actual)

if __name__ == "__main__":
    unittest.main()