'''
Created on 17 Jan 2013

@author: seb10184
'''
import unittest
from events.EventLogCreator import EventLogCreator
from test.BaseTest import BaseTest


class EventLogCreatorTest(BaseTest):

    def test_execute(self):
        self.check_event_processor(EventLogCreator(),
                                   'test_execute',
                                   right_only=['test/events/BugStateFinder.out']
                                   )

if __name__ == "__main__":
    unittest.main()