'''
Created on 9 Jan 2013

@author: seb10184
'''
from os import path
from plugins.GitUpdater import GitUpdater
from test.BaseTest import BaseTest
import unittest

class GitUpdaterTest(BaseTest):

    def test_execute(self):
        self.check_plugin(GitUpdater(),
                          'test_execute',
                          renames={path.join('project_src', 'dot_git'): path.join('project_src', '.git')},
                          right_only=['project_src/dot_git/COMMIT_EDITMSG',
                                      'project_src/dot_git/config',
                                      'project_src/dot_git/description',
                                      'project_src/dot_git/hooks',
                                      'project_src/dot_git/index',
                                      'project_src/dot_git/info',
                                      'project_src/dot_git/logs',
                                      'project_src/dot_git/objects',
                                      'project_src/dot_git/ORIG_HEAD',
                                      'project_src/dot_git/refs'
                                      ] # The only file worth checking is HEAD
                          )

if __name__ == "__main__":
    unittest.main()