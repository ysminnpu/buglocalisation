'''
Created on 9 Jan 2013

@author: seb10184
'''
from os import path
from plugins.Patcher import Patcher
from test.BaseTest import BaseTest
import unittest

class PatcherTest(BaseTest):

    def test_execute(self):
        self.check_plugin(Patcher(),
                          'test_execute',
                          renames={path.join('project_src', 'dot_git'): path.join('project_src', '.git')},
                          right_only=['patch', 'project_src/dot_git'], # Input files we don't need to check
                          )

if __name__ == "__main__":
    unittest.main()