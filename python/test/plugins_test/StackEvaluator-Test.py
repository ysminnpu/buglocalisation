'''
Created on 9 Jan 2013

@author: seb10184
'''
from plugins.StackEvaluator import StackEvaluator
from test.BaseTest import BaseTest
import unittest

class StackEvaluatorTest(BaseTest):

    def test_execute(self):
        self.check_plugin(StackEvaluator(),
                          'test_execute',
                          right_only=['temporary'] # Input files we don't need to check
                          )

    def test_unknown_source(self):
        self.check_plugin(StackEvaluator(),
                          'test_unknown_source',
                          right_only=['temporary'] # Input files we don't need to check
                          )

if __name__ == "__main__":
    unittest.main()