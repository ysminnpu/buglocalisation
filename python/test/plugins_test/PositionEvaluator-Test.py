'''
Created on 9 Jan 2013

@author: seb10184
'''
from plugins.PositionEvaluator import PositionEvaluator
from test.BaseTest import BaseTest
import unittest

class PositionEvaluatorTest(BaseTest):

    def test_execute(self):
        self.check_plugin(PositionEvaluator(),
                          'test_execute',
                          right_only=['123/CorpusEvaluator.out'] # Input file we don't need to check
                          )

if __name__ == "__main__":
    unittest.main()