'''
Created on 9 Jan 2013

@author: seb10184
'''
from plugins.ResultsCombiner import ResultsCombiner
from test.BaseTest import BaseTest
import unittest

class ResultsCombinerTest(BaseTest):

    def test_execute(self):
        self.check_plugin(ResultsCombiner(),
                          'test_execute',
                          right_only=['123/BugLanguageEvaluator.out', '123/CorpusEvaluator.out'] # Input file we don't need to check
                          )

    def test_missing_results(self):
        self.check_plugin(ResultsCombiner(),
                          'test_missing_results',
                          right_only=['123/BugLanguageEvaluator.out'] # Input file we don't need to check
                          )

    def test_missing_first_results(self):
        self.check_plugin(ResultsCombiner(),
                          'test_missing_first_results',
                          right_only=['123/CorpusEvaluator.out'] # Input file we don't need to check
                          )

    def test_missing_all_results(self):
        self.check_plugin(ResultsCombiner(),
                          'test_missing_all_results'
                          )

if __name__ == "__main__":
    unittest.main()