'''
Created on 9 Jan 2013

@author: seb10184
'''
from plugins.TestResultParser import TestResultParser
from test.BaseTest import BaseTest
import unittest

class TestResultParserTest(BaseTest):

    def test_execute(self):
        self.check_plugin(TestResultParser(),
                          'test_execute',
                          right_only=['temporary/TestRunner.out'] # Input file we don't need to check
                          )

    def test_execute_no_test_results(self):
        self.check_plugin(TestResultParser(), 'test_execute_no_test_results')

if __name__ == "__main__":
    unittest.main()