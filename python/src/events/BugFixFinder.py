'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from os import path
from subprocess import Popen, PIPE
import re
from events.EventProcessor import EventProcessor

class BugFixFinder(EventProcessor):
    def load_config(self, args=None, cp=None):
        EventProcessor.load_config(self, args, cp)
        self.bug_pattern = re.compile(self.config['bug_pattern'])
        self.bug_fixes = path.join(self.config['events'], 'BugFixFinder.out')
        self.project_src = path.join(self.config['events'], 'project_src')

    def find_bug_ids(self, message):
        return [match.group(1) for match in self.bug_pattern.finditer(message)]

    def execute(self):
        with open(self.bug_fixes, 'w') as out_file:
            self.write_metadata(out_file)
            proc = Popen(['git', 'log', '--pretty=format:%H %ct %s'], cwd=self.project_src, stdout=PIPE, stderr=PIPE)
            stdout, stderr = proc.communicate()
            if stderr:
                print stderr
            if proc.returncode > 0:
                print 'Process returned error', proc.returncode
                return

            for line in stdout.splitlines():
                if self.debug:
                    print line
                commit, time, message = line.split(' ', 2)
                for bug_id in self.find_bug_ids(message):
                    out_file.write('\t'.join([bug_id, commit, time, message]) + '\n')

if __name__ == '__main__': # pragma: no cover
    bug_fix_finder = BugFixFinder()
    bug_fix_finder.load_config()
    bug_fix_finder.check_project_status()
    bug_fix_finder.execute()
