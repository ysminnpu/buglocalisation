'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from BeautifulSoup import BeautifulSoup, BeautifulStoneSoup
from httplib import HTTPException
from os import path, mkdir
from shutil import rmtree
from urllib2 import urlopen, URLError
import codecs
import sys
from events.EventProcessor import EventProcessor

class DownloadError(Exception): pass
class TransientDownloadError(Exception): pass

class BugDownloader(EventProcessor):
    def load_config(self, args=None, cp=None):
        EventProcessor.load_config(self, args, cp)
        if self.debug:
            print sys.path
        self.bug_parser = __import__(self.config['bug_parser'], fromlist=['parse_bug'])
        self.tracker = self.config['tracker']
        self.product = self.config.get('product')

    def mkdir(self, dirname):
        dirname = path.join(self.events, dirname)
        rmtree(dirname, ignore_errors=True)
        mkdir(dirname)

    def open(self, filename, *args, **kwargs):
        return open(path.join(self.events, filename), *args, **kwargs)

    def execute(self):
        self.mkdir('BugDownloader.summaries')
        self.mkdir('BugDownloader.descriptions')
        report_times = {}
        invalid = {}
        with self.open('BugFixFinder.out') as in_file, \
             self.open('BugDownloader.out', 'w') as confirmed, \
             self.open('BugDownloader.rejected', 'w') as rejected:
            self.write_metadata(confirmed)
            self.write_metadata(rejected)
            for line in in_file:
                if line.startswith('#'): continue
                tries = 0
                repeat = True
                while repeat:
                    repeat = False
                    bug, fix, time, message = line.strip().split('\t', 3)
                    try:
                        if bug in report_times:
                            # Already found and downloaded this bug
                            self.write_success(confirmed, bug, fix, time, report_times[bug], message)
                        elif bug in invalid:
                            # Already attempted to download this bug, but it was not available
                            self.write_failure(rejected, bug, fix, time, message, invalid[bug])
                        else:
                            url = self.tracker % (bug,)
                            if self.debug:
                                print 'Downloading from', url
                            parser = BeautifulSoup(urlopen(url), convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
                            parsed = self.bug_parser.parse_bug(parser)
                            if not parsed:
                                raise DownloadError('No data found')

                            summary, description, product, report_time = parsed
                            if self.product and not self.product == product:
                                raise DownloadError('Incorrect product')
                            report_times[bug] = report_time
                            self.write_field('summary', summary, 'BugDownloader.summaries', bug)
                            self.write_field('description', description, 'BugDownloader.descriptions', bug)
                            self.write_success(confirmed, bug, fix, time, report_time, message)
                    except TransientDownloadError as e:
                        self.write_failure(rejected, bug, fix, time, message, str(e))
                    except (DownloadError, URLError) as e:
                        error = str(e)
                        invalid[bug] = error
                        self.write_failure(rejected, bug, fix, time, message, error)
                    except HTTPException as e:
                        if tries < 3:
                            tries += 1
                            repeat = True
                        else:
                            raise e

    def write_failure(self, rejected, bug, fix, time, message, error):
        rejected.write('\t'.join((bug, fix, time, message, error)) + '\n')

    def write_success(self, confirmed, bug, fix, time, report_time, message):
        if long(report_time) > long(time):
            raise TransientDownloadError('Report time later than fix time')

        confirmed.write('\t'.join((bug, fix, time, report_time, message)) + '\n')

    def write_field(self, name, value, dest, bug):
        if value:
            with codecs.open(path.join(self.events, dest, bug), 'w', 'utf8') as o:
                o.write(value)
        else:
            print 'No', name, 'for', bug

if __name__ == '__main__': # pragma: no cover
    downloader = BugDownloader()
    downloader.load_config()
    downloader.check_project_status()
    downloader.execute()
