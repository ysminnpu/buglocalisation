'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from operator import itemgetter
from os import path
from subprocess import Popen, PIPE
from events.EventProcessor import EventProcessor

def split(contents, splitpoint, separator):
    separated = contents[:splitpoint] + [separator] + contents[splitpoint:]
    return '\t'.join([str(x) for x in separated]) + '\n'

class BugStateFinder(EventProcessor):
    def load_config(self, args=None, cp=None):
        EventProcessor.load_config(self, args, cp)
        self.bug_fixes = path.join(self.events, 'BugDownloader.out')
        self.timed_bug_fixes = path.join(self.events, 'BugStateFinder.out')
        self.project_src = path.join(self.events, 'project_src')

    def execute(self):
        ordered = []
        with open(self.bug_fixes) as in_file:
            for line in in_file:
                if line.startswith('#'): continue
                bug, fix, time, report_time, message = line.strip().split('\t', 4)
                ordered.append([bug, fix, long(time), long(report_time), message])
        ordered.sort(key=itemgetter(3), reverse=True)

        with open(self.timed_bug_fixes, 'w') as out_file:
            self.write_metadata(out_file)
            proc = Popen(['git', 'log', '--pretty=format:%H %ct'], cwd=self.project_src, stdout=PIPE, stderr=PIPE)
            stdout, stderr = proc.communicate()
            if stderr:
                print stderr
            if proc.returncode > 0:
                print 'Process returned error', proc.returncode
                return

            i = 0
            j = 0
            lines = stdout.splitlines()
            while i < len(lines) and j < len(ordered):
                line = lines[i]
                if self.debug:
                    print line
                    print ordered[j]
                commit, time = line.split(' ', 1)
                time = long(time)
                while j < len(ordered) and time < ordered[j][3]:
                    out_file.write(split(ordered[j], 4, commit))
                    j += 1
                i += 1
            while j < len(ordered):
                if self.debug:
                    print ordered[j]
                out_file.write(split(ordered[j], 4, 'NA'))
                j += 1

if __name__ == '__main__': # pragma: no cover
    bug_state_finder = BugStateFinder()
    bug_state_finder.load_config()
    bug_state_finder.check_project_status()
    bug_state_finder.execute()