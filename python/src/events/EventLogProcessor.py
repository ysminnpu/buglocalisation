'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from ConfigParser import NoSectionError, RawConfigParser
from events.EventProcessor import EventProcessor, parse_config
from operator import itemgetter
from os import mkdir, path
from shutil import copy, copytree, rmtree
from subprocess import Popen
import shlex
import sys

class Repeat:
    NONE, COPY, ALL = range(3)

class EventLogProcessor(EventProcessor):
    def load_config(self, args=None, cp=None):
        self.cp = parse_config(args)
        EventProcessor.load_config(self, args, self.cp)
        self.start_event = self.config.get('start_event')
        self.current = self.config['current']
        self.max_events = int(self.config.get('max_events', 0))
        self.event_log = path.join(self.events, 'EventLogCreator.out')
        self.project_src = path.join(self.current, 'project_src')

    def load_plugins(self):
        all_plugins = {}
        for k, v in self.config.items():
            if k.startswith('plugins.'):
                _, event = k.strip().rsplit('.', 1)
                all_plugins[event] = []
                for p in v.split(','):
                    if p:
                        plugin, language = p.strip().rsplit('.', 1)
                        repeat = Repeat.NONE
                        if plugin.startswith('+'):
                            repeat = Repeat.COPY
                            plugin = plugin.lstrip('+')
                        if plugin.startswith('*'):
                            repeat = Repeat.ALL
                            plugin = plugin.lstrip('*')
                        plugin_id = plugin
                        plugin = plugin.split('.', 1)[0]
                        command = self.config['language.' + language]
                        command = command.replace('${plugin}', plugin)
                        path_language = self.config.get('path_' + language)
                        if not path_language:
                            path_language = ''
                        command = command.replace('${path}', path_language)
                        command = command.replace('${config}', self.config['config_file'])
                        all_plugins[event] += [(plugin_id, command, repeat)]
        return all_plugins

    def execute(self):
        self.cp.set('derived', 'metadata.code', self.code_version)
        self.cp.set('derived', 'metadata.results', self.results_version)
        all_plugins = self.load_plugins()
        if self.start_event:
            print 'Starting from event', self.start_event
            if self.start_event == '0':
                self.cp.set('derived', 'initialise', 'true')
                for event in sorted(all_plugins.keys()):
                    for plugin_id, command, _ in all_plugins[event]:
                        self.execute_plugin(plugin_id, command)
                self.cp.set('derived', 'initialise', 'false')
        else:
            rmtree(self.current, ignore_errors=True)
            mkdir(self.current)
            copytree(path.join(self.events, 'project_src'), self.project_src)

        with open(self.event_log) as in_file:
            last_commit = ''
            events_processed = 0
            for line in in_file:
                if line.startswith('#'): continue
                event_id, bug, event, commit, _ = line.strip().split()

                self.update_revision_config(commit)

                self.cp.set('derived', 'bug', bug)
                self.cp.set('derived', 'revision', commit)
                self.cp.set('derived', 'last_revision', last_commit)
                self.cp.set('derived', 'event_id', event_id)
                self.cp.set('derived', 'event', '_'.join((commit, bug, event)))
                self.cp.set('derived', 'metadata.code', self.code_version)
                self.cp.set('derived', 'metadata.results', self.results_version)
                self.write_plugin_config(dict(self.cp.items('derived')), self.config['config_file'])

                if self.start_event:
                    if int(event_id) < int(self.start_event):
                        print 'Fast-forwarding', line
                        continue
                    self.start_event = None

                if self.max_events and events_processed >= self.max_events:
                    print 'Processed', self.max_events, 'events'
                    break

                events_processed += 1

                plugins = all_plugins[event]
                if not plugins:
                    print 'No plugins for', line
                    continue

                if commit == 'NA':
                    print 'Skipping', line
                    continue

                if commit != last_commit:
                    executed = []
                for plugin_id, command, repeat in plugins:
                    if repeat == Repeat.NONE and plugin_id in executed:
                        if self.debug:
                            print 'Already executed', plugin_id
                        continue
                    if repeat != Repeat.COPY or plugin_id not in executed:
                        self.execute_plugin(plugin_id, command)
                        executed += [plugin_id]
                    if repeat == Repeat.COPY:
                        src = path.join(self.current, 'temporary', plugin_id + '.out')
                        dst = path.join(self.current, bug, plugin_id + '.out')
                        if path.exists(src):
                            if path.isdir(src):
                                copytree(src, dst)
                            else:
                                copy(src, dst)
                last_commit = commit

    def execute_plugin(self, plugin_id, command):
        args = shlex.split(command)
        if self.debug:
            print 'Executing', args
        plugin_config = dict(self.cp.items('derived'))
        plugin_config['id'] = plugin_id
        try:
            plugin_config.update(dict(self.cp.items(plugin_id)))
        except NoSectionError:
            pass
        self.write_plugin_config(plugin_config, self.config['config_file'])
        try:
            sys.stdout.flush()
            proc = Popen(args)
            proc.wait()
            if proc.returncode > 0:
                print 'Process returned error', proc.returncode
                raise OSError(proc.returncode, proc.stderr)
        finally:
            sys.stdout.flush()

    def update_revision_config(self, revision):
        config_file = path.join(self.cp.get('events', 'config_dir'), revision + '.config')
        if path.exists(config_file):
            self.cp.read(config_file)

    def write_plugin_config(self, config, config_file):
        parser = RawConfigParser()
        parser.optionxform = str
        parser.add_section('config')
        for k, v in sorted(config.items(), key=itemgetter(0)):
            parser.set('config', k, str(v).replace('%', '%%'))
        with open(config_file, 'w') as f:
            self.write_metadata(f)
            parser.write(f)

if __name__ == '__main__': # pragma: no cover
    event_log_processor = EventLogProcessor()
    event_log_processor.load_config()
    event_log_processor.check_project_status()
    event_log_processor.execute()