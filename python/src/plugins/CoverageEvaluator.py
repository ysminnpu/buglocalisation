'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from BeautifulSoup import BeautifulStoneSoup
from Plugin import Plugin, run
from os import path, listdir
import re

def strip_outer(class_name):
    return class_name[class_name.rfind('$') + 1:]

class CoverageEvaluator(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.method_ids, self.max_method_id = self.parse_method_ids()
        self.coverage_dir = path.join(self.temporary_output, 'TestRunner.out')

    def execute(self):
        if not path.exists(self.coverage_dir):
            self.log('No test results for', self.bug, self.revision)
            return

        ec = []
        em = []
        for f in listdir(self.coverage_dir):
            if f.endswith('.ec'):
                ec += ['-in', path.join(self.coverage_dir, f)]
            if f.endswith('.em'):
                em += ['-in', path.join(self.coverage_dir, f)]
        if not ec:
            self.log('No coverage for', self.bug, self.revision)
            return
        if not em:
            self.log('No metadata for', self.bug, self.revision)
            return

        report = path.join(self.temporary_output, 'CoverageEvaluator.xml')
        command = [
                'java',
                '-cp', self.config['path_java'],
                'emma',
                'report',
                '-r', 'xml',
                '-Dreport.out.file=' + report
                ]
        command += ec
        command += em
        self.execute_and_capture(command, None)

        with open(report) as in_file:
            coverage = self.parse_coverage(in_file)
            out_path = path.join(self.temporary_output, 'CoverageEvaluator.out')
            with open(out_path, 'w') as out_file:
                for method_id, percent in enumerate(coverage):
                    out_file.write('\t'.join((str(method_id), percent)))
                    out_file.write('\n')

    def parse_coverage(self, in_file):
        coverage = ['NA'] * (self.max_method_id + 1)
        parser = BeautifulStoneSoup(in_file, selfClosingTags=['coverage'])
        for package in parser.findAll('package'):
            package_name = package['name']
            for clazz in package.findAll('class'):
                class_name = clazz['name']
                if re.search('\$\d+(\.|\$|$)', class_name):
                    continue
                class_name = class_name.replace('$', '.')
                for method in clazz.findAll('method'):
                    method_name = method['name']
                    if ':' in method_name:
                        method_name = method_name[:method_name.index(':')]
                    if '(' not in method_name:
                        continue
                    method_name, parameters = method_name.split('(')
                    parameters = parameters.rstrip(')').split(',')
                    if '$' in method_name:
                        outer, method_name = method_name.rsplit('$', 1)
                        if outer == 'access' and re.match('\d+', method_name):
                            continue
                        if parameters[0] == outer:
                            parameters = parameters[1:]
                    parameters = [strip_outer(p) for p in parameters]
                    method_name = method_name + ' (' + ','.join(parameters) + ')'

                    name = '.'.join([package_name, class_name, method_name])
                    name = name.replace(' ', '')
                    try:
                        method_id = self.method_ids[name]
                    except KeyError:
                        self.debug('Could not get ID for', name)
                        continue
                    try:
                        score = method.find('coverage', type='block, %')['value']
                        score = score.split()
                        percent = str(1 - float(score[0].strip('%')) / 100.0)
                        coverage[method_id] = percent
                    except:
                        self.log('Unexpected error for', method_id, name)
                        raise
        return coverage

if __name__ == '__main__': # pragma: no cover
    run(CoverageEvaluator())
