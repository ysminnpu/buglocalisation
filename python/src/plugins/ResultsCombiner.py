'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

import errno
from Plugin import Plugin, run
from operator import itemgetter
from os import makedirs, path
from shutil import rmtree

class ResultsCombiner(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.evaluations = [e.strip() for e in self.config['evaluations'].split(',')]

    def execute(self):
        self.results = path.join(self.report_output, 'ResultsCombiner.out')
        in_files = []
        for e in self.evaluations:
            f = None
            try:
                f = open(path.join(self.report_output, e + '.out'))
            except IOError as e:
                if e.errno == errno.ENOENT: pass
                else: raise
            in_files.append(f)
        if any(in_files):
            with open(self.results, 'w') as out_file:
                finished = False
                while not finished:
                    output = ['NA'] * (1 + len(in_files))
                    for i, in_file in enumerate(in_files):
                        if in_file:
                            line = in_file.readline()
                            if len(line) == 0:
                                finished = True
                                break
                            m, output[i + 1] = line.strip().split()
                            if output[0] == 'NA':
                                output[0] = m
                            elif output[0] != m:
                                raise RuntimeError('Non-matching line in ' + in_file.name + '. Expected ' + output[0] + ' but was ' + m)
                    if not finished:
                        out_file.write('\t'.join(output) + '\n')

if __name__ == '__main__': # pragma: no cover
    run(ResultsCombiner())
