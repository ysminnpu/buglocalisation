'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from Plugin import Plugin, run
from os import path, listdir
import re

def parse_value(x):
    try:
        return float(x.strip())
    except ValueError:
        return 0

class SimpleCombineEvaluator(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.in_file = path.join(self.report_output, 'ResultsCombiner.out')
        self.out_file = path.join(self.report_output, self.plugin_id + '.out')
        self.weights = [parse_value(x) for x in self.config['weights'].split(',')]

    def execute(self):
        if not path.exists(self.in_file):
            self.log('No results for', self.bug, self.revision)
            return

        with open(self.in_file) as i, open(self.out_file, 'w') as o:
            for line in i:
                values = line.split('\t')
                method_id, values = values[0], values[1:]
                values = [parse_value(x) for x in values]
                weighted = [a*b for a,b in zip(values, self.weights)]
                total = sum(weighted)
                o.write('\t'.join((method_id, str(total))))
                o.write('\n')

if __name__ == '__main__': # pragma: no cover
    run(SimpleCombineEvaluator())
