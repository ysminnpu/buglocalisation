'''
Created on 28 Nov 2012

@author: seb10184
'''
from ConfigParser import SafeConfigParser
import errno
from os import mkdir, path
from subprocess import Popen, PIPE
import sys

class Plugin(object):
    def __init__(self):
        object.__init__(self)

    def debug(self, *msg):
        if self.log_debug:
            self.log(*msg)

    def log(self, *msg):
        print '[' + self.plugin_id + ']', ' '.join([str(x) for x in msg])

    def init(self):
        pass

    def load_config(self, args=None, cp=None):
        self.config = get_config(args, cp)
        self.current = self.config['current']
        self.plugin_id = self.config['id']
        self.log_debug = self.config.get('debug') == 'true'
        self.initialise = self.config.get('initialise') == 'true'
        self.project_src = path.join(self.current, 'project_src')
        self.temporary_output = path.join(self.current, 'temporary')

        if not self.initialise:
            self.bug = self.config['bug']
            self.revision = self.config['revision']
            self.event_id = self.config['event_id']
            self.report_output = path.join(self.current, self.bug)
            self.fix_output = path.join(self.current, self.bug, self.revision)

    def parse_skips(self, key):
        skips = self.config.get(key, '')
        skips = [x.strip() for x in skips.split(',')]
        return skips

    def execute_and_capture(self, cmd, cwd):
        try:
            self.debug('Executing', *cmd)
            process = Popen(cmd, stdout=PIPE, stderr=PIPE, cwd=cwd)
            stdout, stderr = process.communicate()
            self.debug('>', stdout.replace('\n', '\n> '))
            if stderr:
                self.log('!', stderr.replace('\n', '\n! '))
            if process.returncode:
                raise OSError(process.returncode, 'Process returned error ' + stderr)
            return stdout
        finally:
            sys.stdout.flush()

    def skip_revision(self, revision, skips):
        if revision in skips:
            self.debug('Skipping', revision)
            return True
        return False

    def parse_method_ids(self):
        method_ids = {}
        with open(path.join(self.temporary_output, 'MethodIdentifier.out')) as f:
            for line in f:
                method_id, method, _, event_id = line.split()
                if int(event_id) > self.event_id:
                    self.debug('Skipping unseen method ' + method)
                    continue
                method_ids[method] = int(method_id)
            max_method_id = int(method_id)
        return method_ids, max_method_id

def get_config(args=None, cp=None):
    if not cp:
        if not args:
            args = sys.argv
        config_file = args[1]
        config_file = path.abspath(config_file)
        cp = SafeConfigParser()
        cp.optionxform = str
        if not cp.read(config_file):
            raise IOError('Could not read ' + config_file)
    return dict(cp.items('config'))

def run(plugin, args=None, cp=None):
    plugin.load_config(args, cp)
    if plugin.initialise:
        plugin.init()
    else:
        plugin.execute()