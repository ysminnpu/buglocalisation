'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from Plugin import run
from sklearn import naive_bayes
from SklearnEvaluator import SklearnEvaluator

class NbEvaluator(SklearnEvaluator):
    def __init__(self):
        SklearnEvaluator.__init__(self)

    def load_config(self, args=None, cp=None):
        SklearnEvaluator.load_config(self, args, cp)
        self.model_name = self.config.get('model', 'gaussian')

    def create_model(self):
        if self.model_name == 'gaussian':
            return naive_bayes.GaussianNB()
        elif self.model_name == 'multinomial':
            return naive_bayes.MultinomialNB()
        elif self.model_name == 'bernoulli':
            return naive_bayes.BernoulliNB()
        else:
            raise RuntimeError('Invalid model')

    def get_model_details(self, model):
        if self.model_name == 'gaussian':
            for i, c in enumerate(model.theta_[1]):
                yield (self.evaluations[i] + ' mean', c)
            for i, c in enumerate(model.sigma_[1]):
                yield (self.evaluations[i] + ' variance', c)
        else:
            for i, c in enumerate(model.feature_log_prob_[1]):
                yield (self.evaluations[i] + ' feature log prob', c)

    def predict(self, model, testing):
        return model.predict_proba(testing)[...,1]


if __name__ == '__main__': # pragma: no cover
    run(NbEvaluator())
