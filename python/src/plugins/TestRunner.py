'''
Created on 27 Nov 2012

@author: seb10184
'''
from Plugin import Plugin, run
from collections import defaultdict
from os import getcwd, makedirs, path, remove, walk
from shutil import copyfile
from subprocess import Popen, STDOUT
import errno
import os
import threading
from glob import iglob

class TimeoutError(IOError): pass

def kill_process(proc):
    if proc.poll() == None:
        proc.kill()
        raise TimeoutError('Timeout running tests')

class TestRunner(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.timeout = int(self.config.get('timeout', 60))
        self.skips = self.parse_skips('skip')

    def execute(self):
        self.debug('Running tests for', self.revision)

        if not self.skip_revision(self.revision, self.skips):
            latest = path.join(self.temporary_output, 'TestRunner.out')
            test, cwd, env, clean = self.parse_test_config()
            try:
                if test:
                    cwd = path.join(self.project_src, cwd)
                    self.execute_tests(cwd, test, env)
                    self.copy_results(cwd, latest)
                else:
                    self.debug('No test command')
            except TimeoutError as e:
                self.log(e)
            finally:
                if clean:
                    self.cleanup(clean)

    def execute_tests(self, cwd, test, env):
        environ = os.environ.copy()
        environ.update(env)
        with open(path.join(cwd, 'TestRunner.log'), 'w') as f:
            if self.log_debug:
                self.debug('Executing', test, 'in', cwd, 'with environment')
                for k, v in env.items():
                    self.debug(k, ':', v)
            process = Popen(test, stdout=f, stderr=STDOUT, cwd=cwd, env=environ)
            timer = threading.Timer(self.timeout * 60, kill_process, [process])
            timer.start()
            retcode = process.wait()
            timer.cancel()
            if retcode:
                self.log('Error running tests. Returned', retcode)

    def copy_results(self, cwd, output):
        try:
            makedirs(output)
        except OSError as e:
            if e.errno == errno.EEXIST: pass
            else: raise

        suffixes = {'.ec', '.em', 'TestRunner.log'}
        found = defaultdict(int)
        for root, _, files in walk(cwd):
            for f in files:
                for suffix in suffixes:
                    if f.endswith(suffix):
                        found[suffix] += 1
                        src = path.join(root, f)
                        absolute = path.abspath(src)
                        base = path.abspath(cwd)
                        relative = path.relpath(absolute, base)
                        output_file = relative.replace(os.sep, '_')
                        dst = path.join(output, output_file)
                        copyfile(src, dst)
                        break

        if self.log_debug:
            for suffix in suffixes:
                self.debug(found[suffix], suffix, 'files found')

    def cleanup(self, clean):
        for pattern in clean:
            for f in iglob(pattern):
                remove(f)

    def get_config_value(self, key):
        try:
            value = self.config[key]
        except KeyError:
            return None

        if not value or value == 'NA':
            return None

        value = value.replace('${pwd}', getcwd())
        value = value.replace('${project}', self.config['project_root'])
        return value

    def parse_test_config(self):
        test = self.get_config_value('test')
        if test:
            test = test.split()

        working_dir = self.get_config_value('cwd')
    
        env = self.get_config_value('env')
        environment = {}
        if env:
            for e in env.split(','):
                k, v = e.split('=', 2)
                environment[k.strip()] = v.strip()

        clean = self.get_config_value('clean')
        if clean:
            clean = clean.split(',')

        return test, working_dir, environment, clean

if __name__ == '__main__': # pragma: no cover
    run(TestRunner())
