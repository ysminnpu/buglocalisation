'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from Plugin import Plugin, run
from collections import defaultdict
import errno
from os import makedirs, path
from pyparsing import alphas, alphanums, nums, Word, OneOrMore, Optional, \
    MatchFirst, Regex, SkipTo, Literal, oneOf, Group

class StackEvaluator(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.in_file = path.join(self.temporary_output, 'BugCleaner.stacks.out', self.bug)
        self.out_file = path.join(self.report_output, 'StackEvaluator.out')
        try:
            makedirs(self.report_output)
        except OSError as e:
            if e.errno ==errno.EEXIST: pass
            else: raise

    def execute(self):
        if not path.exists(self.in_file):
            self.log('No stack traces found for', self.bug)
            return

        with open(self.in_file) as f:
            method_ids, max_method_id = self.parse_method_ids()
            compressed_method_ids = self.compress_method_ids(method_ids)
            parser = self.create_parser()
            contents = f.read()
            depths = self.parse_bug(parser, contents, compressed_method_ids)
            self.write_results(depths, max_method_id)

    def create_parser(self):
        exception = Regex('(?i)[a-z][a-z0-9._]*(Exception|Error|Failure)')
        method = Word(alphas + '_<>$', alphanums + '._$<>').setResultsName('method')
        src = Word(alphas + '_', alphanums + '_') + oneOf('.java .jsp')
        line = Word(nums)
        frame = 'at' + method + '(' + MatchFirst([Literal('Unknown') + Literal('Source'), Literal('Native') + Literal('Method'), src + Optional(':' + line)]) + ')'
        return exception + Optional(':' + SkipTo(frame)) + OneOrMore(Group(frame))

    def parse_bug(self, parser, bug, method_ids):
        depths = {}
        for tokens, _, _ in parser.scanString(bug):
            depth = 1
            for token in tokens:
                try:
                    method_name = token['method']
                    method_name = method_name.replace('$', '.')
                    ids = method_ids[method_name]
                    if ids:
                        for method_id in ids:
                            if method_id not in depths:
                                depths[method_id] = 1.0 / depth
                    else:
                        self.log(method_name, 'not found')
                    depth += 1
                except TypeError: pass
                except KeyError: pass
        return depths

    def write_results(self, depths, max_method_id):
        with open(self.out_file, 'w') as o:
            for method_id in range(max_method_id + 1):
                o.write('\t'.join((str(method_id), str(depths.get(method_id, 0)))))
                o.write('\n')

    # This leads to an imprecision, that could be solved if MethodIdentifier recorded
    # line numbers (assuming stack trace has line number information)
    def compress_method_ids(self, method_ids):
        compressed_method_ids = defaultdict(list)
        for method_signature, method_id in method_ids.items():
            method_name = method_signature[0:method_signature.index('(')]
            compressed_method_ids[method_name].append(method_id)
        return compressed_method_ids

if __name__ == '__main__': # pragma: no cover
    run(StackEvaluator())