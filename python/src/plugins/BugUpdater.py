'''
Created on 11 Dec 2012

@author: seb10184
'''
from Plugin import Plugin, run
import errno
from os import makedirs, path
from shutil import copyfile

class BugUpdater(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.repository = path.join(self.config['events'], 'BugDownloader.descriptions')
        self.output = path.join(self.temporary_output, 'BugUpdater.out')

    def execute(self):
        try:
            makedirs(self.output)
        except OSError as e:
            if e.errno == errno.EEXIST: pass
            else: raise
        try:
            copyfile(path.join(self.repository, self.bug), path.join(self.output, self.bug))
        except IOError as e:
            if e.errno == errno.ENOENT:
                self.log('No description for ' + self.bug)
            else:
                raise

if __name__ == '__main__': # pragma: no cover
    run(BugUpdater())