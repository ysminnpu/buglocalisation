'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from Plugin import Plugin, run
from operator import itemgetter
from os import makedirs, path
from shutil import rmtree

class PositionEvaluator(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.evaluations = [e.strip() for e in self.config['evaluations'].split(',')]
        if not self.initialise:
            self.results = path.join(self.fix_output, 'PositionEvaluator.out')

    def execute(self):
        rmtree(self.results, ignore_errors=True)
        makedirs(self.results)
        for e in self.evaluations:
            evaluation = path.join(self.report_output, e + '.out')
            if path.exists(evaluation):
                with open(path.join(self.results, e + '.positions'), 'w') as out_file:
                    methods = []
                    with open(evaluation) as in_file:
                        for line in in_file:
                            method_id, score = line.split()
                            try:
                                score = float(score)
                            except ValueError:
                                score = 0.0
                            methods += [(int(method_id), score)]
                    methods.sort(key=itemgetter(1), reverse=True)
                    methods = [(method[0], position) for (position, method) in enumerate(methods)]
                    methods.sort(key=itemgetter(0))
                    for method_id, position in methods:
                        out_file.write('\t'.join((str(method_id), str(position + 1))) + '\n')
            else:
                self.debug(e, 'results not found')

if __name__ == '__main__': # pragma: no cover
    run(PositionEvaluator())
