package uk.ac.strath.cis.spd.buglanguage.plugins;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.TypeParameter;
import org.eclipse.jface.text.Document;

import uk.ac.strath.cis.spd.buglanguage.listeners.InputListener;
import uk.ac.strath.cis.spd.buglanguage.util.MethodSignature;
import uk.ac.strath.cis.spd.buglanguage.util.MethodParser;

public abstract class Plugin {

    private boolean initialise;
    protected boolean debug;
    protected String bug;
    protected String revision;
    protected String eventId;
    protected String pluginId;
    protected File current, projectSrc, temporaryOutput, fixOutput, reportOutput;

    public static void run(Plugin plugin, String[] args) throws Exception {
        plugin.loadConfig(args);
        if(plugin.initialise) {
            plugin.initialise();
        }
        else {
            plugin.execute();
        }
    }
    public abstract void execute() throws Exception;
    public void initialise() throws Exception {
        // Empty method, can be overridden in sub-classes
    }

    public Properties loadConfig(String[] args) throws IOException {
        Properties config = new Properties();
        config.load(new FileReader(args[0]));
        this.debug = "true".equals(config.getProperty("debug"));
        this.initialise = "true".equals(config.getProperty("initialise"));
        this.pluginId = config.getProperty("id");
        this.current = new File(config.getProperty("current"));
        this.projectSrc = new File(this.current, "project_src");
        this.temporaryOutput = new File(this.current, "temporary");
        if(!this.initialise) {
            this.bug = config.getProperty("bug");
            this.revision = config.getProperty("revision");
            this.eventId = config.getProperty("event_id");
            this.reportOutput = new File(this.current, this.bug);
            this.fixOutput = new File(this.reportOutput, this.revision);
        }
        return config;
    }

    protected String tsv(Object... args) {
        return StringUtils.join(args, "\t");
    }

    protected void debug(Object... msg) {
        if(this.debug) {
            log(msg);
        }
    }

    protected void log(Object... msg) {
        System.out.println("[" + pluginId + "] " + StringUtils.join(msg, " "));
    }

    protected Map<String, MethodSignature> parseMethodIds(File methodIdFile) throws IOException {
        Map<String, MethodSignature> methods = new HashMap<String, MethodSignature>();
        if(methodIdFile.exists()){
            LineIterator lines = FileUtils.lineIterator(methodIdFile);
            int thisEventId = Integer.parseInt(eventId);
            while(lines.hasNext()){
                String line = lines.next();
                String[] parts = line.split("\t");
                int id = Integer.parseInt(parts[0].trim());
                String signature = parts[1].trim();
                String firstSeen = parts[2].trim();
                String firstSeenEventId = parts[3].trim();
                int otherEventId = Integer.parseInt(firstSeenEventId);
                if(otherEventId > thisEventId) {
                    debug("Skipping unseen method " + signature);
                    continue;
                }
                MethodSignature method = new MethodSignature(id, signature, firstSeen, firstSeenEventId);
                methods.put(signature, method);
            }
        }
        return methods;
    }

    protected List<MethodSignature> parseMethodIdList(File methodIdFile) throws IOException {
        Map<String, MethodSignature> methods = parseMethodIds(methodIdFile);
        List<MethodSignature> methodList = new ArrayList<MethodSignature>(methods.values());
        Collections.sort(methodList);
        return methodList;
    }

    protected List<Pattern> parsePatterns(String patternString) {
        List<Pattern> patterns = new ArrayList<Pattern>();
        for(String pattern: patternString.split(",")){
            patterns.add(Pattern.compile(pattern.trim()));
        }
        return patterns;
    }

    protected boolean shouldParseFile(Collection<Pattern> patterns, File root, File file) {
        debug("Deciding whether to parse " + file + " in " + root);
        if(!file.exists()){
            debug(file + " does not exist");
            return false;
        }
        String path = root.toURI().relativize(file.toURI()).getPath();
        for(Pattern pattern: patterns){
            if(pattern.matcher(path).matches()){
                return true;
            }
        }
        debug("No match for " + path);
        return false;
    }

    protected String readFile(File file) throws IOException {
        return FileUtils.readFileToString(file);
    }

    protected void getTypeParameters(List<TypeParameter> parameters, Map<String, String> typeVariables) {
        for(TypeParameter parameter: parameters){
            String type = "Object";
            if(!parameter.typeBounds().isEmpty()){
                type = parameter.typeBounds().get(0).toString();
            }
            typeVariables.put(parameter.getName().toString(), type);
        }
    }

    @SuppressWarnings("unchecked")
    protected String getMethodSignature(String typeName, MethodDeclaration methodDeclaration, Map<String, String> typeVariables) {
        StringBuilder signature = new StringBuilder();
        signature.append(typeName);
        signature.append(".");
        signature.append(methodDeclaration.getName());
        signature.append("(");
        List<SingleVariableDeclaration> parameters = methodDeclaration.parameters();
        if(!parameters.isEmpty()){
            for(SingleVariableDeclaration parameter: parameters){
                String type = parameter.getType().toString();
                if(typeVariables.containsKey(type)){
                    type = typeVariables.get(type);
                }
                if(type.contains("<")){
                    type = type.substring(0, type.indexOf("<"));
                }
                if(type.contains(".")){
                    type = type.substring(type.lastIndexOf(".") + 1);
                }
                signature.append(type);
                if(parameter.isVarargs()){
                    signature.append("[]");
                }
                for(int i = 0; i < parameter.getExtraDimensions(); i++){
                    signature.append("[]");
                }
                signature.append(",");
            }
            signature.deleteCharAt(signature.length() - 1);
        }
        signature.append(")");
        return signature.toString();
    }

    @SuppressWarnings("unchecked")
    private <T> List<T> parseMethods(String source, TypeDeclaration t, String prefix, MethodParser<T> parser) throws IOException {
        if(t.isInterface()){
            return new ArrayList<T>();
        }

        String typeName = t.getName().toString();
        if(prefix != null){
            typeName = prefix + "." + typeName;
        }

        Map<String, String> classTypeVariables = new HashMap<String, String>();
        getTypeParameters(t.typeParameters(), classTypeVariables);

        List<T> results = new ArrayList<T>();

        MethodDeclaration[] methods = t.getMethods();
        for(MethodDeclaration methodDeclaration: methods){
            Map<String, String> typeVariables = new HashMap<String, String>(classTypeVariables);
            getTypeParameters(methodDeclaration.typeParameters(), typeVariables);

            T result = parser.parseMethod(typeName, methodDeclaration, typeVariables, source);
            if(result != null){
                results.add(result);
            }
        }

        TypeDeclaration[] innerTypes = t.getTypes();
        for(TypeDeclaration innerType: innerTypes){
            results.addAll(parseMethods(source, innerType, typeName, parser));
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> parseFile(String input, MethodParser<T> parser) throws IOException {
        List<T> results = new ArrayList<T>();

        Document document = new Document(input);
        ASTParser astParser = ASTParser.newParser(AST.JLS4);
        astParser.setSource(document.get().toCharArray());
        CompilationUnit unit = (CompilationUnit) astParser.createAST(null);

        // to get the imports from the file
        List<TypeDeclaration> types = unit.types();
        for(TypeDeclaration type: types){
            PackageDeclaration typePackage = unit.getPackage();
            String packageName = typePackage != null ? typePackage.getName().toString() : null;
            results.addAll(parseMethods(input, type, packageName, parser));
        }

        return results;
    }

    public <T> void parseFiles(File root, File src, Collection<Pattern> patterns, MethodParser<T> parser) throws IOException {
        File[] listFiles = src.listFiles();
        for(File file: listFiles){
            if(file.isDirectory()){
                if(file.getName().equals(".svn") || file.getName().equals(".git")){
                    continue;
                }
                parseFiles(root, file, patterns, parser);
            }
            else{
                checkAndParseFile(root, file, patterns, parser);
            }
        }
    }

    public <T> void checkAndParseFile(File root, File file, Collection<Pattern> patterns, MethodParser<T> parser) throws IOException {
        if(shouldParseFile(patterns, root, file)){
            parseFile(readFile(file), parser);
        }
    }

    protected void execute(File workingDirectory, InputListener listener, String... command) throws IOException {
        execute(workingDirectory, listener, new HashMap<String, String>(), null, command);
    }

    protected void execute(File workingDirectory, InputListener listener, Integer timeout, String... command) throws IOException {
        execute(workingDirectory, listener, new HashMap<String, String>(), timeout, command);
    }

    protected void execute(File workingDirectory, InputListener listener, Map<String, String> environment, String... command) throws IOException {
        execute(workingDirectory, listener, environment, null, command);
    }

    protected void execute(File workingDirectory, final InputListener listener, Map<String, String> environment, Integer timeout, String... command) throws IOException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);
        try{
            ProcessBuilder builder = new ProcessBuilder(command);
            builder.environment().putAll(environment);
            builder.redirectErrorStream(true);
            builder.directory(workingDirectory);
            final Process process = builder.start();

            Runnable run = new Runnable() {
                @Override
                public void run() {
                    process.destroy();
                }
            };
            if(timeout != null){
                executor.schedule(run, timeout, TimeUnit.MINUTES);
            }

            final InputStream is = process.getInputStream();

            Callable<Void> gobbler = new Callable<Void>() {
                @Override
                public Void call() throws IOException {
                    InputStreamReader isr = new InputStreamReader(is);
                    BufferedReader br = new BufferedReader(isr);
                    String line;
                    while((line = br.readLine()) != null){
                        if(listener != null){
                            listener.processLine(line);
                        }
                        debug("> " + line);
                    }
                    return null;
                }
            };
            Future<Void> gobbled = executor.submit(gobbler);

            int result = process.waitFor();
            gobbled.get();

            if(result != 0){
                throw new IOException("Unexpected result " + result + " executing " + StringUtils.join(command, " "));
            }
        }
        catch(IOException e){
            throw e;
        }
        catch(Exception e){
            throw new IOException(e);
        }
        finally{
            executor.shutdownNow();
        }
    }

    protected List<File> getPreviousBugs(){
        List<File> previousBugs = new ArrayList<File>();
        for(File resultDir: current.listFiles()){
            try {
                int otherBug = Integer.parseInt(resultDir.getName());
                int thisBug = Integer.parseInt(bug);
                if (otherBug >= thisBug) {
                    debug("Skipping bug counts from bug " + otherBug);
                    continue;
                }
            }
            catch(NumberFormatException e) {
                debug("Skipping non-numeric results directory " + resultDir);
                continue;
            }
            previousBugs.add(resultDir);
        }
        return previousBugs;
    }

    protected List<String> getRelatedMethods(File resultDir) throws IOException{
        List<String> relatedMethods = new ArrayList<String>();
        File file = new File(resultDir, "BugMethodFinder.out");
        if(!file.exists()) {
            debug("No method links found for " + resultDir);
        }
        else {
            LineIterator lines = FileUtils.lineIterator(file);
            int thisEventId = Integer.parseInt(eventId);
            while(lines.hasNext()){
                String[] values = lines.nextLine().split("\t");
                String signature = values[0];
                int otherEventId = Integer.parseInt(values[2]);
                if(otherEventId >= thisEventId) {
                    debug("Skipping bug count from event " + otherEventId);
                    continue;
                }
                relatedMethods.add(signature);
            }
        }
        return relatedMethods;
    }

    public static List<String> getChangedFilenames(Properties config) throws IOException{
        File changedFiles = new File(config.getProperty("project"), "GitUpdater.diff");
        List<String> filenames = new ArrayList<String>();
        if(changedFiles.exists()){
            filenames = FileUtils.readLines(changedFiles);
        }
        return filenames;
    }
}
