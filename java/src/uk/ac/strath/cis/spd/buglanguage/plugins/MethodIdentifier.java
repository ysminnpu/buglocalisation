package uk.ac.strath.cis.spd.buglanguage.plugins;

/**
 * See LICENCE_BSD for licensing information.
 *
 * Copyright Steven Davies 2012
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import uk.ac.strath.cis.spd.buglanguage.util.MethodParser;
import uk.ac.strath.cis.spd.buglanguage.util.MethodSignature;

public class MethodIdentifier extends Plugin implements MethodParser<Void> {
    private int lastMethodId;
    private File methodIdFile;
    private Map<String, MethodSignature> methods;
    private List<Pattern> filePatterns;
    private List<String> updatedFiles;

    @Override
    public Void parseMethod(String typeName, MethodDeclaration methodDeclaration, Map<String, String> typeVariables, String source) {
        String signature = getMethodSignature(typeName, methodDeclaration, typeVariables);
        if(!methods.containsKey(signature)){
            lastMethodId++;
            MethodSignature method = new MethodSignature(lastMethodId, signature, revision, eventId);
            methods.put(signature, method);
        }
        return null;
    }

    public void writeOutput() throws IOException {
        PrintWriter out = null;
        try{
            out = new PrintWriter(methodIdFile);
            List<MethodSignature> entries = new ArrayList<MethodSignature>(methods.values());
            Collections.sort(entries);
            for(MethodSignature entry: entries){
                out.println(tsv(entry.getId(), entry.getSignature(), entry.getFirstSeen(), entry.getFirstSeenEventId()));
            }
        }
        finally{
            IOUtils.closeQuietly(out);
        }
    }

    @Override
    public void execute() throws IOException {
        if(!temporaryOutput.exists()) {
            temporaryOutput.mkdir();
        }

        this.lastMethodId = -1;
        this.methods = parseMethodIds(methodIdFile);
        for(MethodSignature method: methods.values()){
            int id = method.getId();
            if(id > lastMethodId){
                this.lastMethodId = id;
            }
        }

        if(updatedFiles != null && !updatedFiles.isEmpty()) {
            for(String filename: updatedFiles){
                File file = new File(this.projectSrc, filename);
                checkAndParseFile(this.projectSrc, file, filePatterns, this);
            }
        }
        else{
            parseFiles(this.projectSrc, this.projectSrc, filePatterns, this);
        }
        writeOutput();
    }

    @Override
    public void initialise() throws Exception {
        if(methodIdFile.exists()) {
            boolean deleted = methodIdFile.delete();
            if(!deleted) {
                throw new IOException("Could not delete " + methodIdFile);
            }
        }
    }

    @Override
    public Properties loadConfig(String[] args) throws IOException {
        Properties config = super.loadConfig(args);
        this.updatedFiles = Plugin.getChangedFilenames(config);
        this.filePatterns = parsePatterns(config.getProperty("file_patterns"));
        this.methodIdFile = new File(temporaryOutput, "MethodIdentifier.out");
        return config;
    }

    public static void main(String[] args) throws Exception {
        run(new MethodIdentifier(), args);
    }
}
