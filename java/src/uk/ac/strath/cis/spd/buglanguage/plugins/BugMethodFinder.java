package uk.ac.strath.cis.spd.buglanguage.plugins;

/**
 * See LICENCE_BSD for licensing information
 *
 * Copyright Steven Davies 2012
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import uk.ac.strath.cis.spd.buglanguage.listeners.DiffInputListener;
import uk.ac.strath.cis.spd.buglanguage.util.Diff;
import uk.ac.strath.cis.spd.buglanguage.util.MethodBody;
import uk.ac.strath.cis.spd.buglanguage.util.MethodParser;

public class BugMethodFinder extends Plugin {
    private File output;
    private List<Pattern> filePatterns;

    public MethodBody[] extractMethods(String input) throws IOException {
        MethodParser<MethodBody> parser = new MethodParser<MethodBody>() {
            @Override
            public MethodBody parseMethod(String typeName, MethodDeclaration methodDeclaration, Map<String, String> typeVariables, String source) {
                String signature = getMethodSignature(typeName, methodDeclaration, typeVariables);

                Block body = methodDeclaration.getBody();
                if(body != null){
                    int startLine = 1 + StringUtils.countMatches(source.substring(0, methodDeclaration.getStartPosition()), "\n");
                    int endLine = startLine + StringUtils.countMatches(source.substring(methodDeclaration.getStartPosition(), body.getStartPosition() + body.getLength()), "\n");
                    return new MethodBody(signature, startLine, endLine);
                }
                return null;
            }
        };
        List<MethodBody> results = parseFile(input, parser);
        Collections.sort(results);
        return results.toArray(new MethodBody[results.size()]);
    }

    private void getChangedMethods(List<Diff> changes) throws FileNotFoundException, IOException {
        String lastFile = null;
        MethodBody[] parsedMethods = new MethodBody[0];
        int i = 0;
        Set<String> methodNames = new HashSet<String>();
        for(Diff change: changes){
            String filename = change.getFile();
            filename = filename.replaceAll("^[ab]/", "");
            int start = change.getFrom(false);
            int end = change.getTo(false);
            debug(filename + ": " + start + " - " + end);
            if(start == 0 || end == 0){
                continue;
            }

            if(!filename.equals(lastFile)){
                lastFile = filename;
                File file = new File(this.projectSrc, filename);
                if(shouldParseFile(filePatterns, this.projectSrc, file)){
                    parsedMethods = extractMethods(readFile(file));
                    debug("Extracted " + parsedMethods.length + " methods");
                    i = 0;
                }
            }

            while(i < parsedMethods.length){
                if(end < parsedMethods[i].getStartLine()){
                    debug(end + " < " + parsedMethods[i].getStartLine());
                    break;
                }

                if(start < parsedMethods[i].getEndLine()){
                    debug(end + " >= " + parsedMethods[i].getStartLine() + " && " + start + " < " + parsedMethods[i].getEndLine());
                    methodNames.add(parsedMethods[i].getSignature());
                }
                i++;
            }
        }
        writeMethodsToFile(methodNames);
    }

    private List<Diff> diff(String previous) throws IOException {
        DiffInputListener listener = new DiffInputListener();
        String[] args = new String[] { "git", "diff", "-U0", previous };
        execute(this.projectSrc, listener, args);
        return listener.getChanges();
    }

    @Override
    public void execute() throws IOException {
        List<Diff> changes = diff(revision + "^");
        getChangedMethods(changes);
    }

    private void writeMethodsToFile(Set<String> methods) throws IOException {
        if(!methods.isEmpty()){
            List<String> methodsToWrite = new ArrayList<String>(methods);
            Collections.sort(methodsToWrite);

            PrintWriter out = null;
            try{
                out = new PrintWriter(FileUtils.openOutputStream(this.output, true));
                for(String method: methodsToWrite){
                    out.println(tsv(method, revision, eventId));
                }
            }
            finally{
                IOUtils.closeQuietly(out);
            }
        }
    }

    @Override
    public void initialise() throws Exception {
        for(File report: this.current.listFiles()) {
            File results = new File(report, "BugMethodFinder.out");
            if(results.exists()) {
                boolean deleted = results.delete();
                if(!deleted) {
                    throw new IOException("Could not delete old result " + results);
                }
            }
        }
    }

    @Override
    public Properties loadConfig(String[] args) throws IOException {
        Properties config = super.loadConfig(args);
        this.filePatterns = parsePatterns(config.getProperty("file_patterns"));
        this.output = new File(reportOutput, "BugMethodFinder.out");
        return config;
    }

    public static void main(String[] args) throws Exception {
        run(new BugMethodFinder(), args);
    }
}
