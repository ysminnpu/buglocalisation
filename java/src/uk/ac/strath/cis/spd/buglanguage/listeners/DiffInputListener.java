/**
 * See LICENCE_BSD for licensing information
 *
 * Coyright Steven Davies 2012
 */
package uk.ac.strath.cis.spd.buglanguage.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.ac.strath.cis.spd.buglanguage.util.Diff;

public class DiffInputListener implements InputListener {
    private static final Pattern filePattern = Pattern.compile("^\\+\\+\\+\\s+(.+)(?:\\s+\\(.+\\))?$");
    private static final Pattern hunkPattern = Pattern.compile("^@@\\s+-(\\d+)(?:,(\\d+))?\\s+\\+(\\d+)(?:,(\\d+))?\\s+@@.*$");

    private List<Diff> changes;
    private String file;

    public DiffInputListener() {
        this.changes = new ArrayList<Diff>();
    }

    @Override
    public void processLine(String line) {
        Matcher matcher = filePattern.matcher(line);
        if(matcher.matches()){
            file = matcher.group(1);
        }
        else{
            matcher = hunkPattern.matcher(line);
            if(matcher.matches()){
                changes.add(new Diff(file, matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4)));
            }
        }
    }

    public List<Diff> getChanges() {
        return changes;
    }
}
